package esn.config;

import esn.service.impl.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import java.util.*;

@Configuration
public class SecurityConfig implements WebMvcConfigurer {


    @Autowired
    UserDetailsServiceImpl userDetailsService;
    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    public SimpleUrlHandlerMapping customFaviconHandlerMapping() {
        SimpleUrlHandlerMapping mapping = new SimpleUrlHandlerMapping();
        mapping.setOrder(Integer.MIN_VALUE);

        Map<String, ResourceHttpRequestHandler> map = new HashMap<>();
        map.put("/favicon.ico",faviconRequestHandler());
        map.put("/templates/**",templatesRequestHandler());

        mapping.setUrlMap(map);

        return mapping;
    }

    @Bean
    protected ResourceHttpRequestHandler faviconRequestHandler() {
        ResourceHttpRequestHandler requestHandler
                = new ResourceHttpRequestHandler();
        ClassPathResource classPathResource
                = new ClassPathResource("static/");
        List<Resource> locations = new ArrayList<>();
        locations.add(classPathResource);
        requestHandler.setLocations(locations);
        return requestHandler;
    }

    @Bean
    protected ResourceHttpRequestHandler templatesRequestHandler() {
        ResourceHttpRequestHandler requestHandler
                = new ResourceHttpRequestHandler();
        List<Resource> locations = new ArrayList<>();
        locations.add( new ClassPathResource("classpath:/templates"));
        locations.add( new ClassPathResource("classpath:/static/"));
        locations.add( new ClassPathResource("templates/"));
        requestHandler.setLocations(locations);
        return requestHandler;
    }


    @Bean
    public SecurityFilterChain configure(HttpSecurity http) throws Exception {
        http.csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(
                        request -> request
                                .requestMatchers("/user/register", "/templates/**")
                                .permitAll()
                                .requestMatchers(
                                        PathRequest
                                                .toStaticResources()
                                                .atCommonLocations())
                                .permitAll().anyRequest().authenticated()
                                )


                .formLogin((form) -> form
                        .loginPage("/login")
                        .failureUrl("/login-error.html")
                        .permitAll()
                )
                .logout(httpSecurityLogoutConfigurer -> httpSecurityLogoutConfigurer.permitAll()
                        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                        .logoutSuccessUrl("/login"));

        return http.build();
    }



}

