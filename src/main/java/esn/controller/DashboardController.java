package esn.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DashboardController {
    @RequestMapping("/")
    public String dashboard(final Model model) {
        model.addAttribute("numberEmployee",400);
        model.addAttribute("numberEngineer",300);
        return "dashboard.html";
    }
}
