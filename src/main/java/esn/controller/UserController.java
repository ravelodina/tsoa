package esn.controller;

import esn.controller.formvalidation.UserDto;
import esn.entity.User;
import esn.service.UserService;
import esn.service.impl.UserServiceImpl;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder encoder;

    @PostMapping(path = "/register")
    private String register(@Valid @ModelAttribute("userDto")  UserDto userDto, BindingResult bindingResult, final Model model){

        if (bindingResult.hasErrors() || !userDto.getPassword().equals(userDto.getPasswordConfirm())) {
            if (!userDto.getPassword().equals(userDto.getPasswordConfirm())){
                bindingResult.rejectValue("passwordConfirm", null, "Passwords don't match");
            }
            return "register.html";
        }else{
            User userInBase = userService.findUserByUsername(userDto.getUsername());
            if(userInBase!= null){
                bindingResult.rejectValue("username", null, "User already exist");
                return "register.html";
            }else{
                User user = new User();
                user.setUsername(userDto.getUsername());
                user.setPassword(encoder.encode(userDto.getPassword()));
                userService.addUser(user);
                return "redirect:/login";
            }
        }
    }

    @RequestMapping("/register")
    public String register(@ModelAttribute UserDto userDto, Model model) {
        model.addAttribute("userDto", userDto);
        return "register.html";
    }


}
