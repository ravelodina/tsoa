package esn.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/employee")
public class EmployeeController {
    @RequestMapping("/engineer")
    public String listEngineer(final Model model) {
        return "engineer.html";
    }

    @RequestMapping("/manager")
    public String listManager(final Model model) {
        return "manager.html";
    }
}
