package esn.service;

import esn.entity.User;

public interface UserService {
    public User addUser (User user);
    public User findUserByUsername(String userName);
}
