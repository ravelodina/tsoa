package esn.service.impl;

import esn.repository.UserRepository;
import esn.entity.User;
import esn.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User addUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findUserByUsername(String userName) {
        Optional<User> optional = userRepository.findByUsername(userName);
        return optional.orElse(null);
    }
}
