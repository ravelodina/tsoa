package esn.entity;



import esn.entity.enums.Speciality;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Entity(name="employees")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="employee_type",
        discriminatorType = DiscriminatorType.INTEGER)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    @Id
    private long id;
    private String lastName;
    private String firstName;
    private LocalDate hiringDate;
    private LocalDate startExpDate;
    private LocalDate birthDate;
    //...
    @ManyToOne
    @JoinColumn(name="company_id", nullable=false)
    private Company company;
}
