package esn.entity.enums;

public enum Speciality {
    JAVA,
    NET,
    PHP,
    DEV_OPS,
}
